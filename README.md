# holub_compiler

This repository contains the implementation of a simple recursive descent parser for all forms of primitive C declarations including variables, pointers, pointers to functions, function declarations themselves, and arrays.